<?php

/* $connection = mysqli_connect('localhost','root', '1223',); */

/* if(!connection){ */
/*   echo "Error: Unable to connect to MySQL." . PHP_EOL; */
/*   echo "Debgging errno: " . mysqli_connect_errno() . PHP_EOL; */
/*   echo "Debugging error: " . mysqli_connect_errno() . PHP_EOL; */
/* } */

/* $server = 'localhost'; */
/* $user = 'root'; */
/* $password = '123'; */
/* $db = 'php-cms'; */

/* // connecting to database via PHP */
/* $mysqli = new mysqli($server, $user, $password, $db); */

/* if (mysqli_connect_errno()) trigger_error(mysqli_connect_error()); */

/* 
 * Using PDO
 */

$host = 'localhost';
$dbname = 'php-login';
$user = 'root';
$password = '123';

try {
  $connection = new PDO("mysql:host=$host;dbname=$dbname", $user, $password);
  $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  echo "connected successfully";
}
catch (PDOException $e) {
  echo "Connection failed: " . $e->getMessage();
}
?>
