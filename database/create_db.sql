CREATE TABLE 'users' {
  'id' int(11)) NOT NULL,
  'name' varchar(255) NOT NULL,
  'email' varchar(255) NOT NULL,
  'password' varchar(255) NOT NULL,
  'role' varchar(255) NOT NULL,
  'active' tinyint(1) NOT NULL DEFAULT '0' 
} ENGINE=InnoDB DEFAULT CHARSET=latin1;

---
--Dumping data for table 'users'
---

INSERT INTO 'users' ('id', 'name', 'email', 'password', 'role', 'active') VALUES (1, 'marcel', 'fivedragons5@gmail.com', '59f2443a4317918ce29ad28a14e1bdb7', 'admin', 1);

ALTER TABLE 'users'
  ADD PRIMARY KEY('id');

ALTER TABLE 'users'
  MODIFY 'id' int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

---
-- Table structure for table 'category'

CREATE TABLE 'category' (
  'id' int(11) NOT NULL,
  'name' varchar(255) NOT NULL,
  'description' varchar(255) NOT NULL,
  'image' varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

---
-- Dumping data for table 'category'
---

INSERT INTO 'category' ('id', 'name', 'caturl', 'description', 'image') VALUES
(3, 'Web Design', 'web-design', 'Web Design Description', ''),
(4, 'Web Development', 'web-development', 'Web Development Description', ''),
(5, 'Mobile App Development', 'mobile-app-development', 'Mobile App Development Decription', '');

ALTER TABLE 'category'
  ADD PRIMARY KEY ('id');
ALTER TABLE 'category'
  MODIFY 'id' int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

---
-- Table structure for table 'content'
---

CREATE TABLE 'content' (
  'id' int(11) NOT NULL,
  'catid' varchar(255) NOT NULL,
  'subcatid' varchar(255) NOT NULL,
  'title' varchar(255) NOT NULL,
  'content' longtext NOT NULL,
  'url' varchar(255) NOT NULL,
  'status' varchar(255) NOT NULL,
  'createtime' datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  'updatetime' datetime NOT NULL,
  'publishtime' datetime NOT NULL,
  'featuredimage' varchar(255) NOT NULL,
  'userid' int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE 'content'
  ADD PRIMARY KEY ('id');

ALTER TABLE 'content'
  MODIFY 'id' int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

---
-- Table structure for table 'comments'
---

CREATE TABLE 'comments' (
  'id' int(11) NOT NULL,
  'cid' int(11) NOT NULL,
  'name' varchar(255) NOT NULL,
  'email' varchar(255) NOT NULL,
  'subject' varchar(255) NOT NULL,
  'submittime' datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  'status' varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE 'comments'
  ADD PRIMARY KEY ('id');

ALTER TABLE 'comments'
  ADD 'id' int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

---
-- Table structure for table 'settings'
---

CREATE TABLE 'settings' (
  'id' int(11) NOT NULL,
  'name' varchar(255) NOT NULL,
  'value' varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

---
-- Dumping data for table 'settings'
---

INSERT INTO 'settings' ('id', 'name', 'values') VALUES
(1, 'logo', 'img/site-logo.png'),
(2, 'ad', 'your ad code here'),
(3, 'perpage', '2'),
(4, 'sitename', 'Dragons House');

ALTER TABLE 'settings'
  ADD PRIMARY KEY ('id');

ALTER TABLE 'settings'
  MODIFY 'id' int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
